from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'taxane_nmr.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'nmr.views.home', name='home'),
    url(r'^search$', 'nmr.views.search', name='search'),
    url(r'^results/([0-9a-z]+)$', 'nmr.views.results', name='results'),
    url(r'^downloads$', 'nmr.views.downloads', name='downloads'),
    url(r'^browse$', 'nmr.views.browse', name='browse'),
    url(r'^record/([0-9]+)', 'nmr.views.record', name='record'),
    url(r'^help$', 'nmr.views.help', name='help'),
    url(r'^submit$', 'nmr.views.submit', name='submit'),
    url(r'^thank_you$', 'nmr.views.thank_you', name='thank_you'),
    #url(r'^admin/', include(admin.site.urls)),
)
