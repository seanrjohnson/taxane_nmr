"""
WSGI config for taxane_nmr project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""
from django.core.management import call_command
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "taxane_nmr.settings")

##### STARTUP PRE-INIT #####
#call_command('initialize_query_server')


##### start the wsgi server like normal #####
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
