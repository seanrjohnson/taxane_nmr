"""
Django settings for taxane_nmr project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from .private_settings import EMAIL_USE_TLS, EMAIL_HOST, EMAIL_HOST_USER, EMAIL_HOST_PASSWORD, EMAIL_PORT, ADMINS, SPECTRUM_SUBMISSION_RECEIVERS
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '9p*8r9unhkh$iy!(13uhala@b&nev7-2)a16az#p@a^2*_z20!'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['localhost', '127.0.0.1', 'langelabtools.wsu.edu']

QUERY_WORKERS = 2 #Number of workers that should be started to execute searches (i.e. number of concurrent searches possible)
KEY_QUERY_SERVER_PORT = 'QUERY_SERVER_PORT' #where in the Setting database table the QUERY_SERVER_PORT setting is stored
RESULTS_PAGE_RELOAD_INTERVAL = 5 #how many seconds to wait between results page refreshes
DAYS_TO_KEEP_RESULTS = 10 #delete results after this many days
QUERIES_BETWEEN_CLEANING = 100 #after this many queries, run the database cleaning routine to remove expired queries and results
MAX_HITS_TO_DISPLAY = 50 #after this many queries, run the database cleaning routine to remove expired queries and results
#temporary files directory
TEMPFILE_DIRECTORY = "/tmp/nmr_temp" #os.path.join(BASE_DIR, '../temp')


# Application definition

INSTALLED_APPS = (
    #'django.contrib.admin',
    #'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'nmr',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'taxane_nmr.urls'

WSGI_APPLICATION = 'taxane_nmr.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/nmr/static/'
STATIC_ROOT = os.path.join(BASE_DIR, '../static')


