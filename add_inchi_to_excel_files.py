from openpyxl import Workbook, load_workbook
from glob import glob
import subprocess

input_directory = "excel_files"
output_directory = "new_excel_files"
mols_directory = "E:/taxane_NMR/records/mols"
inchi_program_path = "E:/programs/INCHI-1-BIN/win32/inchi-1.exe"

file_names = glob(input_directory + "/*.xlsx")

def main():

    for input_name in file_names:
        wb = load_workbook(input_name)
        ws = wb['notes']
        first_empty = 0
        fields = dict()
        while (True):
            val = ws.cell(row = first_empty, column = 0).value
            if val == "" or val is None:
                break
            else:
                fields[ws.cell(row = first_empty, column = 0).value] = ws.cell(row = first_empty, column = 1).value
                first_empty += 1
        
        molstring = get_molstring(mols_directory + "/" + str(fields['Lab #'])+".mol")
        
        (inchi,inchikey) = make_inchi_strings(molstring)

        
        ws.cell(row = first_empty, column = 0).value = "InChI"
        ws.cell(row = first_empty, column = 1).value = inchi
        
        ws.cell(row = first_empty+1, column = 0).value = "InChIKey"
        ws.cell(row = first_empty+1, column = 1).value = inchikey
        
        
        
        wb.save(output_directory+"/"+str(fields['Lab #'])+".xlsx")

        
def get_molstring(file_name):
    mol_string = ""
    with open(file_name, "rb") as infile:
        lines = infile.readlines()
        for line in lines:
            line = line.rstrip()
            # if len(line) == 21:
                # mol_string += line[0:15]
            # else:
            mol_string += line
            mol_string += "\n"
    return(mol_string)


def make_inchi_strings(molstring):
    
    #exec_string = 'molconvert inchi:"AuxNone Key"' ### For molconvert
    exec_string = inchi_program_path + " " + "/STDIO /AuxNone /Key"
    
    process = subprocess.Popen(exec_string,  stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = process.communicate(molstring)
    if err:
        print("InChI conversion error" + " " + molstring + " " + err)
    fields = out.splitlines()
    inchi = fields[1]
    inchikey = fields[2]
    
    return (inchi, inchikey)
    

if __name__ == "__main__":
    main()