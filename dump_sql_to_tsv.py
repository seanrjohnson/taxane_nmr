from __future__ import unicode_literals
import sqlite3
from pandas.io import sql
# Create your connection.
cnx = sqlite3.connect('db.sqlite3')
table_names_to_output_names = {'nmr_proton':'proton.tsv', 'nmr_carbon':'carbon.tsv', 'nmr_record':'record.tsv', 'nmr_protoncoupling':'proton_coupling.tsv'}

for (table_name, out_name) in table_names_to_output_names.items():
    table = sql.read_sql("SELECT * FROM %s;" % table_name, cnx)
    table.to_csv(out_name, sep="\t", index=False, encoding='utf-8')
