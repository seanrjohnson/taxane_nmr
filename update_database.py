from openpyxl import Workbook, load_workbook
from glob import glob
import subprocess
import os
import re
import stat
from taxane_nmr import settings
'''
  NOTE: molconvert should be in the shell PATH
'''

data_directory = r'/home/sean/Desktop/non_naproc_files'
svg_output_path = r'/taxane_nmr/taxane_nmr/nmr/static/nmr/svg'#should be the server media directory
xlsx_output_path = r'/taxane_nmr/taxane_nmr/excel_files' #should be the server xlsx directory
inchi_program_path = r'/opt/inchi/inchi-1'

django_base_directory = r'/taxane_nmr/taxane_nmr' 
python_executable = r'python3' #python3 executable path

xl_files = dict()
mol_files = dict()

debug = False #If true then only do one file

def main():
  for f in glob(os.path.join(data_directory,r'*.xlsx')):
    
    m = re.search('^(.+)\.xlsx$', os.path.basename(f))
    if m:
      xl_files[m.group(1)] = f
      
  for f in glob(os.path.join(data_directory, r'*.mol')):
    
    m = re.search('^(.+)\.mol$', os.path.basename(f))
    if m:
      mol_files[m.group(1)] = f

  #~ print(mol_files.keys())
  #~ print(xl_files.keys())

  diff = set(mol_files.keys()).symmetric_difference(xl_files.keys())

  if len(diff) > 0:
    print("Some records don't have both mol files and excel files: " + " ".join(diff))
    
    for d in diff:
      if d in mol_files:
        del mol_files[d]
      if d in xl_files:
        del xl_files[d]
  

#### Generate Images ####
  for (record, path) in mol_files.items():
    if not os.path.isfile(os.path.join(svg_output_path, record)+".svg"):
      exec_string = 'molconvert svg:"w350 h350 chiral_all" %s -o %s' % (path, os.path.join(svg_output_path, record)+".svg")
      process = subprocess.Popen(exec_string, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
      out, err = process.communicate()
      if err:
        print(f + " " + str(err))
      if debug == True:
        break

#### add InChI to xls file ####
  for (record, path) in xl_files.items():
    wb = load_workbook(path)
    ws = wb['notes']
    first_empty = 1
    fields = dict()
    while (True):
      val = ws.cell(row = first_empty, column = 1).value
      if val == "" or val is None:
        break
      else:
        fields[ws.cell(row = first_empty, column = 1).value] = ws.cell(row = first_empty, column = 1).value
        first_empty += 1
    if ("InChI" not in fields) or ("InChIKey" not in fields):
      molstring = get_molstring(mol_files[record])
      (inchi,inchikey) = make_inchi_strings(molstring)
        
      if "InChI" not in fields:
        ws.cell(row = first_empty, column = 1).value = "InChI"
        ws.cell(row = first_empty, column = 2).value = inchi
        first_empty += 1
      if "InChIKey" not in fields:
        ws.cell(row = first_empty, column = 1).value = "InChIKey"
        ws.cell(row = first_empty, column = 2).value = inchikey
      
      #wb.save(path)
    if not os.path.isfile(os.path.join(xlsx_output_path, record + ".xlsx")):
      wb.save(os.path.join(xlsx_output_path, record + ".xlsx"))
    
##### update the database #####
  os.chdir(django_base_directory)
  
  exec_string = python_executable + " manage.py syncdb"
  process = subprocess.Popen(exec_string, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
  out, err = process.communicate()
  if err:
    print("Error running syncdb" + " " + err.decode('ASCII'))

  exec_string = python_executable + " make_db.py"
  process = subprocess.Popen(exec_string, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
  out, err = process.communicate()
  if err:
    print("Error running make_db.py" + " " + err.decode('ASCII'))
    
##### change database permissions #######
  os.fchmod(settings.DATABASES["default"]["NAME"], st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)
  
  
def get_molstring(file_name):
    mol_string = ""
    with open(file_name, "r") as infile:
        lines = infile.readlines()
        for line in lines:
            line = line.rstrip()
            # if len(line) == 21:
                # mol_string += line[0:15]
            # else:
            mol_string += line
            mol_string += "\n"
    return(mol_string)


def make_inchi_strings(molstring):
    
    #exec_string = 'molconvert inchi:"AuxNone Key"' ### For molconvert
    #exec_string = inchi_program_path + " " + "/STDIO /AuxNone /Key"
    exec_string = inchi_program_path + " " + "-STDIO -AuxNone -Key"
    
    process = subprocess.Popen(exec_string,  stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = process.communicate(bytes(molstring, 'ASCII'))
    print(molstring)
    #if err:
    #    print("InChI conversion error" + " " + molstring + " " + err.decode('ASCII'))
    fields = out.splitlines()
    inchi = fields[1]
    inchikey = fields[2]
    
    return (inchi, inchikey)
    
    
if __name__ == "__main__":
    main()
