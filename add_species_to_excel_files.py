from openpyxl import Workbook, load_workbook
from glob import glob
import subprocess

input_directory = "compound_data"
output_directory = "new_compound_data"
file_names = glob(input_directory + "/*.xlsx")
species_name = "Taxus"

def main():

    for input_name in file_names:
        wb = load_workbook(input_name)
        ws = wb['notes']
        first_empty = 0
        fields = dict()
        while (True):
            val = ws.cell(row = first_empty, column = 0).value
            if val == "" or val is None:
                break
            else:
                fields[ws.cell(row = first_empty, column = 0).value] = ws.cell(row = first_empty, column = 1).value
                first_empty += 1
        
        ws.cell(row = first_empty, column = 0).value = "Species"
        ws.cell(row = first_empty, column = 1).value = species_name

        wb.save(output_directory+"/"+str(fields['Lab #'])+".xlsx")

        
if __name__ == "__main__":
    main()