#import sqlite3
from __future__ import print_function
from __future__ import unicode_literals
import re
import glob
import xlrd
import codecs
import os
import sys
from nmr import util
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "taxane_nmr.settings")
from django.conf import settings
from nmr.models import Record, Carbon, Proton, ProtonCoupling
from django.db.transaction import commit_on_success


@commit_on_success #this makes this program go about 100 times faster
def main():
    # first find all of the xlsx files in the directory
    
    file_names = glob.glob("excel_files/*.xlsx")
    
    #sheets_to_db = {'notes':'records', 'proton':'proton', 'carbon':'carbon'} #keys are sheet names, values are database table names
    #sheet_orientations = ['notes':, 'proton', 'carbon'] #row means col 0 is the label (the data of the same type is in the same row), and other rows are data. col means row 0 is the headings and
    sheet_names = ['notes', 'proton', 'carbon']
    # other rows are data (data in a given column is all the same type)
    
    data_to_db = dict() #keys are sheet names, values are dicts of sheet_label -> db_label associations
    #data_to_db['notes'] = {'Lab #': 'lab_num',	'CAS #': 'cas_num',	'common name':'common_name', 	'synonym':'synonym',	'IUPAC name':'iupac_name',	'Formula':'formula',	'Molecular weight':'molecular_weight',	'Exact mass': 'exact_mass',	'NMR reference(s)':'nmr_references',	'NMR solvent':'nmr_solvent',	'MHz':'mhz',	'Calibration':'calibration',	'Notes':'notes'}
    data_to_db['proton'] = {'position':'position', 'ppm':'ppm', 'peak type':'peak_type'}
    data_to_db['carbon'] = {'position':'position', 'ppm':'ppm'}
    data_to_db['notes'] = {"Lab #": "lab_num","CAS #": "CAS_num","common name": "common_name", "Common name": "common_name", "Common Name": "common_name",  "synonym": "synonym", "Synonym": "synonym", "Synonyms": "synonym", "Formula": "formula","Molecular weight": "molecular_weight","Molecular Weight": "molecular_weight" ,"Exact mass": "exact_mass", "Exact Mass": "exact_mass" ,"NMR reference(s)": "nmr_references","NMR solvent": "nmr_solvent","MHz": "mhz","Calibration": "calibration", "Species": "species", "Notes": "notes", "InChI": "inchi_standard", "InChIKey": "inchi_key"}
     #a preprocessing function to call on data before saving to the database (can probably also do this with django functions)
    notes_functions = {'formula': lambda x: util.normalize_formula(x),
        'lab_num': lambda x: str(int(float(x))),
        'nmr_references': lambda x: x.replace("\n", "")}
    J_col = 'J (Hz)'
    
    for file in file_names:
        try:
        #print(file, file=sys.stderr)
            rec_name = re.search(r'([^\\]+)\.xlsx', file).group(1)
            wb = xlrd.open_workbook(file)
            sheets = set(wb.sheet_names())
            if not set(sheet_names).issubset(sheets):
                print("cannot process, incorrect sheets in file: %s " % file)
                continue
            notes = wb.sheet_by_name('notes')
            new_rec = Record()
            for i in range(notes.nrows):
                if notes.cell(i,0).value in data_to_db['notes']:
                    val = notes.cell(i,1).value
                    field_name = data_to_db['notes'][notes.cell(i,0).value]
                    if field_name in notes_functions:
                        val = notes_functions[field_name](val)
                    setattr(new_rec, field_name, val)
            # wait until the end to actually add this data to rec_data so that we can get the number of peaks as well
            if not util.check_formula(new_rec.formula, new_rec.exact_mass):
                print("Warning: mass and formula do not correspond for compound %s: %s %.3f" % (new_rec.lab_num, new_rec.formula, new_rec.exact_mass))
            new_rec.save()
            
            protons = wb.sheet_by_name('proton')
            col_names = [x.value for x in protons.row(0)]
            proton_peaks = 0;
            for n in range(1,protons.nrows):
                new_prot = Proton()
                new_prot.record = new_rec
                jays = list()
                for (col_num, col_name) in enumerate(col_names):
                    if col_name in data_to_db['proton']:
                        if len(protons.row(n)) >= col_num: #make sure we're not calling an out of bounds cell
                            setattr(new_prot, data_to_db['proton'][col_name], protons.row(n)[col_num].value)
                        else:
                            setattr(new_prot, data_to_db['proton'][col_name], None)
                    elif col_name == J_col:
                        if len(protons.row(n)) >= col_num: #make sure we're not calling an out of bounds cell
                            jays = [x.strip() for x in str(protons.row(n)[col_num].value).split(',') if x.strip() != '']
                    else:
                        print("unexpected proton column name in data file: %s" % file)
                if (new_prot.ppm is not None) and (new_prot.ppm != ""):
                    new_prot.order = proton_peaks
                    proton_peaks += 1
                    try:
                        new_prot.save()
                        for x in jays:
                            new_j = ProtonCoupling()
                            new_j.proton = new_prot
                            new_j.j = x
                            new_j.save()
                    except:
                        print(file + " database error proton " + repr(protons.row(n)) + sys.exc_info()[0] , file=sys.stderr)
            
            
            carbons = wb.sheet_by_name('carbon')
            col_names = [x.value for x in carbons.row(0)]
            carbon_peaks = 0;
            for n in range(1,carbons.nrows):
                new_carb = Carbon()
                new_carb.record = new_rec
                for (col_num, col_name) in enumerate(col_names):
                    if col_name in data_to_db['carbon']:
                        if len(carbons.row(n)) >= col_num:
                            setattr(new_carb, data_to_db['carbon'][col_name], carbons.row(n)[col_num].value)
                        else:
                            setattr(new_carb, data_to_db['carbon'][col_name], None)
                    else:
                        print("unexpected carbon column name in data file: %s" % file)
                if (new_carb.ppm is not None) and (new_carb.ppm != ""):
                    new_carb.order = carbon_peaks
                    carbon_peaks += 1
                    new_carb.save()
            new_rec.proton_peaks = proton_peaks
            new_rec.carbon_peaks = carbon_peaks
            new_rec.save()
        except:
            print(file + " database error " + str(sys.exc_info()[0]), file=sys.stderr)
        

        
if __name__ == '__main__':
    main()
