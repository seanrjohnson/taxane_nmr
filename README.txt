Django app Dependencies:
pyzmq 14.1.0 (zmq version 4.0.4)
django 1.6.x
numpy
pandas
xlrd
openpyxl 1.8.2

utility scripts:
process_molfiles.py: generates images and InChI and SMILES strings from molfiles. Relies on ChemAxon molconvert. Also relies on ChemDraw
dump_sql_to_tsv.py: Reads the database and dumps its contents to a
generate_secret_key.py: generates a random secret key for you to paste into your taxane_nmr/settings.py file.
make_db.py: reads excel files and adds their contents to the database.

Custom management commands:
execute_query: deprecated, don't use this
initialize_query_server: Starts a zmq server to accept and run database queries
kill_query_server: kills the active query server. Using this command allows you to kill only the query server without having to restart the webserver.
submit_nmr_query: connects to the query server and sends it a query ID to put in its queue.