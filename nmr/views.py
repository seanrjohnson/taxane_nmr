from django.shortcuts import render, redirect
import time
from nmr.models import Record, ProtonCoupling, Query, Result
from django.forms.models import model_to_dict
from pprint import pprint
from nmr.forms import QueryForm, SubmitForm
from django.core.management import call_command
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.conf import settings 
import pickle
from django.contrib import messages
from django.core.mail import EmailMessage
from nmr import util
import re
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from operator import itemgetter
#from pprint import pprint
def _base_dict(page):
    return {'year': str(time.gmtime().tm_year), 'page':[page,]}
    
    
def home(request):
    template_vars = _base_dict('Home')
    return render(request, 'nmr/home.html', template_vars)

def submit(request):
    template_vars = _base_dict('Submit')
    error = None
    if (request.POST):
        form = SubmitForm(request.POST)
    else:
        form = SubmitForm()
    if request.method == 'POST':
        if form.is_valid():
            instance = form.save()
            filename = util.write_record_to_xlsx(instance, settings.TEMPFILE_DIRECTORY)
            targets = [settings.EMAIL_HOST_USER]
            if settings.DEBUG == False:
                targets += settings.SPECTRUM_SUBMISSION_RECEIVERS
            email_msg = EmailMessage("NMR spectrum submission", "", settings.EMAIL_HOST_USER, to=targets)
            if filename is not None:
                try:
                    email_msg.attach_file(filename)
                    message = "Successfully generated xlsx from submission. Excel file attached attached."
                except:
                    message = "Someone tried to submit a spectrum, but it could not be attached to this email. check the server temporary directory."
            else:
                message = "Someone tried to submit a spectrum, but the conversion to excel format failed. Check the database on the server."
            message += "\n\nSubmitter: " + instance.submitter_name
            message += "\n Submitter E-mail: " + instance.submitter_email
            email_msg.body = message
            email_msg.send(fail_silently=False)
            return redirect('thank_you')
    template_vars['form'] = form
    template_vars['error'] = error
    return render(request, 'nmr/submit.html', template_vars)

def thank_you(request):
    template_vars = _base_dict('Thank_you')
    return render(request, 'nmr/thank_you.html', template_vars)
    
def results(request, query_number):
    template_vars = _base_dict('Results')
    template_vars['reload_interval'] = settings.RESULTS_PAGE_RELOAD_INTERVAL
    template_vars['max_hits'] = settings.MAX_HITS_TO_DISPLAY
    
    # check if the query has been submitted and if the results have been returned
    try:
        query = Query.objects.get(pk=query_number)
    except ObjectDoesNotExist:
        query = None
    try:
        result = Result.objects.get(pk=query_number)
    except ObjectDoesNotExist:
        result = None
    
    status = 'Never submitted'
    if result:
        status = 'result'
    elif query:
        status = 'query'
    print(status)
    template_vars['status'] = status
    if status == 'result':
        
        result_data = pickle.loads(result.data)
        if request.method == 'GET' and 'sort' in request.GET:
            if request.GET['sort'] == 'avg':
                result_data = sorted(result_data, key=lambda x: x['score'], reverse=True)
            elif request.GET['sort'] == 'prot':
                result_data = sorted(result_data, key=lambda x: x['proton_score'], reverse=True)
            elif request.GET['sort'] == 'carb':
                result_data = sorted(result_data, key=lambda x: x['carbon_score'], reverse=True)
            elif request.GET['sort'] == 'mass':
                result_data = sorted(result_data, key=lambda x: x['exact_mass_score'], reverse=True)

        template_vars['result_data'] = result_data
        template_vars['result_errors'] = pickle.loads(result.errors)

    return render(request, 'nmr/results.html', template_vars)

def browse(request):
    template_vars = _base_dict('Browse')

    sort_dir = False
    sort_name = 'lab_num'

    if request.method == 'GET' and 'sort' in request.GET and 'dir' in request.GET:
        if request.GET['dir'] == 'hl':
            sort_dir = True
        
        
        if request.GET['sort'] == 'name':
            sort_name = 'common_name'
        elif request.GET['sort'] == 'form':
            sort_name = 'formula'
        elif request.GET['sort'] == 'mass':
            sort_name = 'exact_mass'
        elif request.GET['sort'] == 'cas':
            sort_name = 'CAS_num'
        elif request.GET['sort'] == 'prot':
            sort_name = 'proton_peaks'
        elif request.GET['sort'] == 'carb':
            sort_name = 'carbon_peaks'
        elif request.GET['sort'] == 'inchi':
            sort_name = 'inchi_key'
    
    ####PAGINATION#####
    
        
    recs = util.values_to_dict(Record.objects.values())
    
    for rec in recs:
        rec['lab_num'] = int(rec['lab_num'])
    
    recs = sorted(recs, key=itemgetter("lab_num"))
    
    paginator = Paginator(recs, 50)
    
    page_num = request.GET.get('page')
    
    try:
        recs = paginator.page(page_num)
    except PageNotAnInteger:
        recs = paginator.page(1)
        page_num=1
    except EmptyPage:
        recs = paginator.page(paginator.num_pages)
        page_num=paginator.num_pages
    
    template_vars['page_num'] = recs.number
    template_vars['num_pages'] = recs.paginator.num_pages
    total_pages = template_vars['num_pages']


    #[model_to_dict(x) for x in recs.object_list]
    #recs = [model_to_dict(x) for x in recs.object_list]
    
    #####/PAGINATION#####
    
    #recs = recs[0:50 ]
    
    recs = sorted(recs, key=itemgetter(sort_name), reverse = sort_dir)
    
    template_vars['records'] = recs
    
    #pagiation bar
    try:
        int_page = int(page_num)
        if total_pages > 5:
            if int_page >=5 and int_page<=(total_pages-2):
                template_vars['page_navigator'] = [int_page-2,int_page-1,int_page,int_page+1,int_page+2]
            elif int_page >(total_pages - 2):
                template_vars['page_navigator'] = [total_pages-4,total_pages-3,total_pages-2,total_pages+-1,total_pages]
            else:
                template_vars['page_navigator'] = [1,2,3,4,5]
        else:
            template_vars['page_navigator'] = range(1, total_pages+1)
    except:
        if total_pages <5:
            template_vars['page_navigator'] = range(1, total_pages+1)
        else:
            template_vars['page_navigator'] = [1,2,3,4,5]
    
    #previous and next arrows    
    try:
        previous_pages = int_page
        next_pages = int_page
        if total_pages>5:
            if int_page>=6 and int_page<=(total_pages-5):
                previous_pages-=5
                next_pages+=5
            elif int_page<6 and total_pages>5:
                next_pages+=5
            elif int_page>(total_pages-5):
                previous_pages-=5
            else:
                next_pages = int_page
                previous_pages = int_page
        else:
            next_pages = int_page
            previous_pages = int_page
    
    #probably don't need this
    except:
        previous_pages=1
        next_pages=1 #hacky fix next_pages = 6
        
    
    template_vars['previous_pages'] = previous_pages
    template_vars['next_pages'] = next_pages
        
    return render(request, 'nmr/browse.html', template_vars)
    
def record(request, record_number):
    template_vars = _base_dict('Record')
    err = None
    rec = None
    rec_dict = None
    proton_table = None
    carbon_table = None
    try:
        rec = Record.objects.get(pk=record_number)
        rec.split_references = re.sub('\.\ ?;','.<br /><br />',rec.nmr_references)
        #rec_dict = model_to_dict(rec)
        carbon_table = rec.carbon_set.all().values() #TODO: probably a good idea to sort these in some way
        protons = rec.proton_set.all()
        proton_table = util.values_to_dict(protons.values()) #make it into a real genuine list of dicts
        for (i, p) in enumerate(protons):
            proton_table[i]['j'] = ", ".join([str(x['j']) for x in p.protoncoupling_set.all().values()]) #or I could just make a __str__ function for ProtonCoupling
    except:
        err = "Record not found"
    
    template_vars['proton_table'] = proton_table
    template_vars['carbon_table'] = carbon_table
    template_vars['record'] = rec
    #template_vars['record_dict'] = rec_dict
    template_vars['error'] = err
    
    return render(request, 'nmr/record.html', template_vars)

def search(request):
    template_vars = _base_dict('Search')
    error = None
    
    default_field_values = {
            'name':'','exact_mass': '', 'chemical_formula': '', 'proton_shift_tolerance': '0.05', 'carbon_shift_tolerance': '0.3', 'coupling_constant_tolerance': '0.6', 'exact_mass_tolerance': '0.01', 
            'proton_nmr': '', 'carbon_nmr': '',
        }
    example_field_values = {
            'name':'','exact_mass': '853.335', 'chemical_formula': 'C47H51NO14', 'proton_shift_tolerance': '0.05', 'carbon_shift_tolerance': '0.3', 'coupling_constant_tolerance': '0.6', 'exact_mass_tolerance': '0.01', 
            'proton_nmr': '1.98\tbr s\t\n5.67\td\t7.1\n3.79\tdd\t7.0, 1.0\n2.38\ts\t\n4.94\tdddd\t9.6, 2.3, 0.9, 0.8\n2.54\tddd\t14.8, 9.7, 6.7\n1.88\tddd\t14.7, 11.0, 2.3\n4.40\tdd\t10.9, 6.7\n2.48\tbr s\t\n6.27\ts\t\n2.23\ts\t\n6.23\tq\t9.0, 1.5\n2.35\tdd\t15.4, 9.0\n2.28\tddd\t15.3, 9.0, 0.6\n1.14\ts\t\n1.24\ts\t\n1.79\td\t1.5\n1.68\ts\t\n4.30\tddd\t8.4, 1.1, 0.8\n4.19\tdd\t8.5, 1.0\n4.78\td\t2.7\n3.61\tbr s\t\n5.78\tdd\t8.9, 2.8\n7.01\td\t8.9\n8.13\tdd\t8.4, 1.3\n7.51\tm\t\n7.61\ttt\t7.4, 1.4\n7.48\tm\t\n7.42\tm\t\n7.35\ttt\t7.3, 1.6\n7.74\tdd\t8.3, 1.2\n7.40\tm\t\n7.49\tm\t\n'
            , 'carbon_nmr': '79.00\n74.90\n45.60\n81.10\n84.40\n35.60\n72.20\n58.60\n203.60\n75.50\n133.20\n142.00\n72.30\n35.70\n43.20\n21.80\n26.90\n14.80\n9.50\n76.50\n172.70\n73.20\n55.00\n170.40\n22.60\n171.20\n20.80\n129.10\n129.10\n130.20\n128.71\n133.70\n133.60\n127.03\n128.68\n131.90\n167.02\n138.00\n127.04\n129.00\n128.30\n',
    }
    
    if (request.POST):
        query_form = QueryForm(request.POST)
    else:
        query_form = QueryForm(initial = default_field_values)
    
    if request.method == 'POST':
        if query_form.is_valid():
            query_instance = query_form.save()
            call_command('submit_nmr_query', query_instance.hash)
            return redirect('results/%s' % (query_instance.hash,))
    template_vars['form'] = query_form
    template_vars['error'] = error
    template_vars['default_fields'] = util.jsonify_dict(default_field_values, '#id_')
    template_vars['example_fields'] = util.jsonify_dict(example_field_values, '#id_')
    return render(request, 'nmr/search.html', template_vars)

def downloads(request):
    template_vars = _base_dict('Downloads')
    return render(request, 'nmr/downloads.html', template_vars)

def help(request):
    template_vars = _base_dict('Help')
    return render(request, 'nmr/help.html', template_vars)
