# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines for those models you wish to give write DB access
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from django.db import models
from django.core.urlresolvers import reverse
from random import randint
from datetime import datetime, timedelta
from hashlib import sha1
import re
from nmr import util
from django.conf import settings
from django.utils import timezone
from django.core.exceptions import ValidationError
from decimal import Decimal

def normalize_int_string(int_string):
    int_string = str(int_string) #will give horrible errors on python 2.7
    out = int_string
    m = re.match('^([0-9]+)(\.0*)?$',int_string)
    if m:
        out = m.group(1)
    return out

class Setting(models.Model):
    #this table stores settings that can be changed at runtime
    name = models.CharField(max_length=25, blank=False, unique=True, primary_key=True)
    value = models.CharField(max_length=50, blank=True)

class Record(models.Model):
    lab_num = models.CharField(max_length=25, blank=False, unique=True, help_text='The internal id assigned to the record by the Lange lab', primary_key=True)
    CAS_num = models.CharField(max_length=25, blank=True, help_text='Chemical Abstracts Service (CAS) number of the molecule')
    inchi_key = models.CharField(max_length=36, blank=False, help_text='InChIKey, 27 character identifier for the compound')
    inchi_standard = models.CharField(max_length=1000, blank=False, help_text='Standard InChI, a unique identifier for the structure')
    common_name = models.CharField(max_length=1000, blank=True)
    synonym = models.CharField(max_length=5000, blank=True)
    formula = models.CharField(max_length=1000, blank=True)
    molecular_weight = models.DecimalField(blank=False, max_digits=28, decimal_places=10)
    exact_mass = models.DecimalField(blank=False, max_digits=28, decimal_places=10)
    nmr_references = models.TextField()
    nmr_solvent = models.CharField(max_length=1000, blank=True)
    mhz = models.CharField(max_length=1000, blank=False)
    calibration = models.TextField()
    species = models.TextField()
    notes = models.TextField()
    proton_peaks = models.IntegerField(null=True)
    carbon_peaks = models.IntegerField(null=True)
    def get_absolute_url(self):
        return reverse('record', args=[self.lab_num])

class Submission(models.Model):
    submitter_name = models.CharField(max_length=1000, blank=False, help_text='Your name')
    submitter_email = models.EmailField(max_length=254, blank=False, help_text='Your e-mail address')
    
    common_name = models.CharField(max_length=1000, blank=True, help_text='The name most commonly used for the molecule.')
    synonym = models.CharField(max_length=5000, blank=True, help_text='Other names used for the molecule.')
    inchi = models.CharField(max_length=5000, blank=False, help_text='Full InChI, does not have to be Standard InChI.')
    formula = models.CharField(max_length=1000, blank=False, help_text='The molecule\'s chemical formula.')
    nmr_references = models.TextField(blank=True, help_text='Preferably in the style of the journal Cell. If a different reference is used for the proton spectrum as for the carbon spectrum, please put the proton reference first, then a semicolon, then the carbon reference.')
    nmr_solvent = models.CharField(max_length=1000, blank=True, help_text='If a different solvent was used for the proton spectrum as for the carbon spectrum, please put the proton solvent first, then a semicolon, then the carbon solvent.')
    mhz = models.DecimalField(max_length=1000, max_digits=20, decimal_places=5, blank=True, null=True, help_text='Proton NMR mHz.')
    calibration = models.CharField(max_length=1000, blank=True, help_text='What was the basis for shift calibration.')
    notes = models.TextField(blank=True, help_text='Additional notes about the spectra or molecule.')
    proton_spectrum = models.TextField(blank=True, help_text='Assignments, shifts, peak types, and coupling constants for proton the proton spectrum.')
    carbon_spectrum = models.TextField(blank=True, help_text='Assignments, and shifts for the carbon spectrum.')

class Carbon(models.Model):
    record = models.ForeignKey(Record)
    position = models.CharField(max_length=1000, blank=True)
    ppm = models.DecimalField(blank=False, max_digits=20, decimal_places=5)
    order=models.PositiveSmallIntegerField(blank=False)
    def save(self, *args, **kwargs):
        #convert decimal values into flat integers
        self.position = normalize_int_string(self.position)
        super(Carbon, self).save(*args, **kwargs)
    class Meta:
        unique_together = ("record", "order")

class Proton(models.Model):
    record = models.ForeignKey(Record)
    position = models.CharField(max_length=1000, blank=True)
    ppm = models.DecimalField(blank=False, max_digits=20, decimal_places=5)
    peak_type = models.CharField(max_length=20, blank=True)
    peak_type_search_format = models.TextField(max_length=20, blank=True)
    order=models.PositiveSmallIntegerField(blank=False)
    class Meta:
        unique_together = ("record", "order")
    def save(self, *args, **kwargs):
        #convert decimal values into flat integers
        self.position = normalize_int_string(self.position)
        self.peak_type = self.peak_type.strip()
        self.peak_type_search_format = util.format_peak_type_for_search(self.peak_type)
        super(Proton, self).save(*args, **kwargs)

class ProtonCoupling(models.Model):
    proton = models.ForeignKey(Proton)
    j = models.DecimalField(blank=False, max_digits=20, decimal_places=5)
    def __str__(self):
        return '%s' % (self.j,)

    
class Query(models.Model):
    #Basically, how the query will work is if you enter data for a particular field, it will use that data to search
    ALIGNMENT_ALGORITHM_CHOICES = ((x,x) for x in ('munkres'))
    ALIGNMENT_SCORING_CHOICES = (('PURE', 'Pure'),
                                ('MIXTURE', 'Mixture'))
    
    
    proton_shift_tolerance = models.FloatField(blank=True, null=True)
    carbon_shift_tolerance = models.FloatField(blank=True, null=True)
    coupling_constant_tolerance = models.FloatField(blank=True, null=True)
    exact_mass = models.CharField(blank=True, null=True, max_length=5000)
    exact_mass_tolerance = models.DecimalField(blank=True, null=True, max_digits=20, decimal_places=10)
    chemical_formula = models.CharField(max_length=5000, blank=True)
    hash = models.CharField(max_length=40, unique=True, primary_key=True) #40 is the length of a sha1 hash hex
    alignment_algorithm = models.CharField(max_length=20, blank=False, default='munkres', choices=ALIGNMENT_ALGORITHM_CHOICES)
    alignment_scoring = models.CharField(max_length=10, blank=False, default='PURE', choices=ALIGNMENT_SCORING_CHOICES) #If PURE, penalize an alignment score based on max(query_peaks, target_peaks). If MIXTURE, penalize alignment score based on target_peaks.
    name = models.CharField(max_length=5000, blank=True)
    
    proton_nmr = models.TextField(blank=True) #three columns, ppm, peak type, J (Hz)
    carbon_nmr = models.TextField(blank=True) #one column, peak type
    def clean(self):

        if self.exact_mass != None and self.exact_mass != "":
            try: 
                for num in self.exact_mass.split(";"):
                    Decimal(num.strip())
            except Exception:
                raise ValidationError('Invalid exact mass: should be semicolon separated decimal numbers, e.g.: "342.74;1456.7", or "342.74"')
        

    def save(self, *args, **kwargs):
        #generate hash for search
        #x = 
        #print(x)
        #print(type(x))
        #print(type(x.encode('ascii')))
        #print(x.encode('ascii'))
        #print("".join([str(randint(0,9)) for x in range(4)]))
        bytes = datetime.now().isoformat() + "".join([str(randint(0,9)) for x in range(4)])
        bytes = bytes.encode('ascii')
        self.hash = sha1(bytes).hexdigest()
        #normalize_chemical_formulas
        formulas = [util.normalize_formula(x.strip()) for x in self.chemical_formula.split(';')]
        self.chemical_formula = ";".join([x for x in formulas if x != ""])
        
        
        self.proton_nmr = self.proton_nmr.strip()
        self.carbon_nmr = self.carbon_nmr.strip()
        #print(self.hash)
        super(Query, self).save(*args, **kwargs)


class Result(models.Model):
    query = models.ForeignKey(Query, unique=True)
    hash = models.CharField(max_length=40, unique=True, primary_key=True) #40 is the length of a sha1 hash hex
    data = models.BinaryField(blank=True) # a pickled output of the query results (can't be json because it contains decimal fields)
    expires = models.DateTimeField(blank=False)
    errors = models.BinaryField(blank=True) # a pickled list of errors encountered during the search (could be json, but I'm trying to stay consistent)
    def get_absolute_url(self):
        return reverse('result', args=[self.hash])
    
    def save(self, *args, **kwargs):
        self.expires = timezone.now() + timedelta(days = settings.DAYS_TO_KEEP_RESULTS)
        super(Result, self).save(*args, **kwargs)
