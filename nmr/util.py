import re
from decimal import Decimal
from pprint import pprint
from datetime import datetime
from openpyxl import Workbook
import os
from collections import OrderedDict
from .elements import ELEMENTS
import json

def catenate_coupling_constant_lists(list_of_coupling_constant_lists):
    return [", ".join([str(y) for y in x]) for x in list_of_coupling_constant_lists]

def values_to_dict(query_set_values):
    '''
        convert a QuerySet.values() "list of dicts" into a real genuine list of dicts
    '''
    return [x for x in query_set_values.values()]

def format_peak_type_for_search(peak_type):
    return peak_type.replace("br","").strip()
    
def string_to_formula(input):
    '''
        converts a string representing a molecular formula into a dict where keys are element abbreviations and values are stoichiometry
        adatped from the Spektraris Perl function stringToFormula
        
        returns an empty dict if the formula could not be parsed
    '''
    out = dict()
    
    input = "".join(input.split()) #remove all whitespaces
    
    while (len(input) > 0):
        match = re.match('([A-Z][a-z]?)([0-9]*)(.*)$',input)
        if match:
            num = 0
            if (match.group(2) != ''):
                num = int(match.group(2))
            else:
                num = 1
            out[match.group(1)] = int(out.get(match.group(1), 0)) + num
            input = match.group(3)
        else:
            out = dict()
            #print "warning: chemical formula $in not parsable"
            break
    return out


def formula_to_string(form):
    '''
        converts a formula dict to a string representing a molecular formula, where elements are listed in alphabetical order
        adatped from the Spektraris Perl function formulaToString
        
    '''
    out = '' 
    for k in sorted(form.keys()):
        out = out + k + str(form[k])
    
    return out


def normalize_formula(form):
    '''
        converts a chemical formula to a unique representation
    '''
    
    return formula_to_string(string_to_formula(form))

def check_formula(form, mass, tolerance=0.001):
    '''
        return True if the exact mass calculated from the formula is equal to the mass passed, within a distance if tolerance
        else return False
    '''
    mass = float(mass)
    std_masses={"C":12.0, "H":1.007825035, "O":15.99491463, "N":14.003074, "P":30.973762, "S":31.9720707}
    form_dict = string_to_formula(form)
    form_mass = sum([std_masses[k]*float(v) for (k,v) in form_dict.items()])
    
    match = True
    if mass > form_mass + tolerance or mass < form_mass - tolerance:
        print(mass)
        print(form_mass)
        print(form)
        match = False
    
    return match
    
def parse_proton_query(in_str):
    '''
        takes a proton nmr query entry and splits it into a list of dicts with keys: ppm, peak_type, j
        ppm is a Decimal, peak type is a string, j is a list of Decimals
        Input should be lines of 1 or more fields, delimited by tabs or pipes.
        ppm    peak_type    j
        
        returns: list_of_errors, list of dicts
    '''
    err = list()
    out = list()
    lines=in_str.split("\n")
    for line in lines:
        line=line.strip()
        fields = re.split('\||\t',line)
        ppm=""
        peak_type=""
        j=""
        if len(fields) > 0:
            ppm = fields[0].strip()
        if len(fields) > 1:
            peak_type = format_peak_type_for_search(fields[1]) #remove "broad" designation
        if len(fields) > 2:
            j = fields[2].strip().split(",")
        if ppm != "":
            try:
                ppm=float(ppm)
            except:
                err.append("could not understand proton ppm: %s" % fields[0])
            if type(ppm) == type(float()): #if it's a float, then it must have parsed correctly
                
                if j != "":
                    try:
                        j = sorted([float(x) for x in j])
                    except:
                        err.append("could not understand proton coupling constants: %s" % fields[2])
                out.append({'ppm':ppm, 'peak_type':peak_type, 'j':j})
    return err, out

def parse_carbon_query(in_str):
    '''
        takes a carbon nmr query entry and splits it into a list of floats
        Input should be lines of one number.
        ppm
        
        returns: list_of_errors, list of ppms
    '''
    err = list()
    out = list()
    
    lines=in_str.split("\n")
    for line in lines:
        line=line.strip()
        if line != "":
            try:
                line=float(line)
            except:
                err.append("could not understand carbon ppm: %s" % line)
            if type(line) == type(float()): #if it's a float, then it must have parsed correctly
                out.append(line)
    return err, out

def parse_proton_submission(in_str, submission=False):
    '''
        takes a proton nmr query entry and splits it into a list of dicts with keys: ppm, peak_type, j
        ppm is a Decimal, peak type is a string, j is a list of Decimals
        Input should be lines of 2 or more fields, delimited by tabs or pipes.
        position    ppm    peak_type    j
        
        returns: list_of_errors, list of dicts
    '''
    err = list()
    out = list()
    lines=in_str.split("\n")
    for line in lines:
        line=line.strip()
        fields = re.split('\||\t',line)
        position = ""
        ppm=""
        peak_type=""
        j=""
        if len(fields) > 0:
            position = fields[0].strip()
        if len(fields) > 1:
            ppm = fields[1].strip()
        if len(fields) > 2:
            peak_type = fields[2]
        if len(fields) > 3:
            j = fields[3].strip().split(",")
        if ppm != "":
            try:
                ppm=float(ppm)
            except:
                err.append("could not understand proton ppm: %s" % fields[0])
            if type(ppm) == type(float()): #if it's a float, then it must have parsed correctly
                
                if j != "":
                    try:
                        j = ",".join(["%.3f" % (x) for x in sorted([float(x) for x in j], reverse=True)])
                    except:
                        err.append("could not understand proton coupling constants: %s" % fields[2])
                out.append({'position': position, 'ppm':ppm, 'peak type':peak_type, 'J (Hz)':j})
    return err, out
    
def parse_carbon_submission(in_str):
    '''
        takes a carbon nmr query entry and splits it into a list of dicts with keys 'position' and 'ppm'
        Input should be lines of two columns where the left column is position and the right column is shift
        
        returns: list_of_errors, dict
    '''
    err = list()
    out = list()
    
    lines=in_str.split("\n")
    for line in lines:
        line=line.strip()
        fields = re.split('\||\t',line)
        position = ""
        ppm=""
        if len(fields) > 0:
            position = fields[0].strip()
        if len(fields) > 1:
            ppm = fields[1].strip()
        if ppm != "":
            try:
                ppm=float(ppm)
            except:
                err.append("could not understand carbon ppm: %s" % fields[0])
            if type(ppm) == type(float()): #if it's a float, then it must have parsed correctly
                out.append({'position': position, 'ppm':ppm})
    return err, out
    
    
def exact_mass_from_formula(form):
    form_dict = string_to_formula(form)
    mass = 0.0
    for (elem, stoich) in form_dict.items():
        if elem in ELEMENTS:
            mass += ELEMENTS[elem].monoisotopic_mass * float(stoich)
    return mass

def molecular_weight_from_formula(form):
    form_dict = string_to_formula(form)
    mass = 0.0
    for (elem, stoich) in form_dict.items():
        if elem in ELEMENTS:
            mass += ELEMENTS[elem].exactmass * stoich
    return mass
    
def write_record_to_xlsx(submission, target_directory):
    '''
        reads a Submission model instance and writes the data it contains into an excel (xlsx) file
        returns the file path of the written file
    '''
    submission_string =  re.sub('[:\- .]', '', str(datetime.now()))
    filename = os.path.abspath(os.path.join(target_directory, submission_string + ".xlsx"))
    
    notes_fields = OrderedDict([
                ("Lab #", lambda x: submission_string), 
                ("InChI", lambda x: x.inchi),
                ("common name", lambda x: x.common_name), 
                ("synonym", lambda x: x.synonym), 
                ("Formula", lambda x: normalize_formula(x.formula)), 
                ("Molecular weight", lambda x: molecular_weight_from_formula(x.formula)),
                ("Exact mass", lambda x: exact_mass_from_formula(x.formula)),
                ("NMR reference(s)", lambda x: x.nmr_references), 
                ("NMR solvent", lambda x: x.nmr_solvent), 
                ("MHz", lambda x: x.mhz), 
                ("Calibration", lambda x: x.calibration), 
                ("Notes", lambda x: x.notes), 
                ])
    
    wb = Workbook()
    
    notes = wb.create_sheet(title="notes")
    proton = wb.create_sheet(title="proton")
    carbon = wb.create_sheet(title="carbon")
    
    notes_row = 0
    for (field, extractor) in notes_fields.items():
        notes.cell(row = notes_row, column = 0).value = field
        notes.cell(row = notes_row, column = 1).value = extractor(submission)
        notes_row += 1
    
    proton_headings = ('position', 'ppm', 'peak type', 'J (Hz)')
    for (col, heading) in enumerate(proton_headings):
        proton.cell(row=0, column=col).value = heading
    
    (proton_errors, proton_peaks) = parse_proton_submission(submission.proton_spectrum)
    for (peak_num, peak_dict) in enumerate(proton_peaks):
        for (col, heading) in enumerate(proton_headings):
            proton.cell(row=1+peak_num, column=col).value = peak_dict[heading]
    
    
    carbon_headings = ('position', 'ppm')
    for (col, heading) in enumerate(carbon_headings):
        carbon.cell(row=0, column=col).value = heading
    
    (carbon_errors, carbon_peaks) = parse_carbon_submission(submission.carbon_spectrum)
    for (peak_num, peak_dict) in enumerate(carbon_peaks):
        for (col, heading) in enumerate(carbon_headings):
            carbon.cell(row=1+peak_num, column=col).value = peak_dict[heading]
    
    notes.cell(row = notes_row, column = 0).value = 'Errors'
    notes.cell(row = notes_row, column = 1).value = ";".join(proton_errors + carbon_errors)
    
    try:
        #remove the default sheet
        wb.remove_sheet(wb.get_sheet_by_name('Sheet'))
        
        #save the file
        wb.save(filename)
    except:
        filename = None
    
    return filename
    
def jsonify_dict(indict, key_prefix=""):
    '''
        input: dict, prefix (optional)
        
        makes a new dict with the keys of the original except with the prefix prepended.
        Keeps the same values
        returns the dict as a json string
    '''
    new_dict = {}
    
    for (k, v) in indict.items():
        new_dict[key_prefix + k] = v
    return json.dumps(new_dict)
