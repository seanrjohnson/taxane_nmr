from django import forms
from nmr.models import Query, Submission


query_widgets = {'exact_mass': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'exact_mass_tolerance': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'chemical_formula': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'proton_shift_tolerance': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'carbon_shift_tolerance': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'coupling_constant_tolerance': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'alignment_scoring': forms.Select(attrs={'class':"form-control"}),
            'proton_nmr': forms.Textarea(attrs={'class':"form-control"}),
            'carbon_nmr': forms.Textarea(attrs={'class':"form-control"}),
            'name':forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            }
query_fields = ('name','exact_mass', 'exact_mass_tolerance', 'chemical_formula', 'proton_shift_tolerance', 'carbon_shift_tolerance', 'coupling_constant_tolerance', 'alignment_scoring', 'proton_nmr', 'carbon_nmr')
#query_widgets = {x: 
class QueryForm(forms.models.ModelForm):
    
    class Meta:
        model = Query
        fields = query_fields
        widgets = query_widgets
        labels = {
            'name':'Name','proton_shift_tolerance': 'Proton Shift Tolerance', 'carbon_shift_tolerance': 'Carbon Shift Tolerance', 'coupling_constant_tolerance': 'Coupling Constant Tolerance', 'exact_mass': 'Exact Mass', 'chemical_formula': 'Chemical Formula', 
            'proton_nmr': 'Proton NMR', 'carbon_nmr': 'Carbon NMR', 'alignment_scoring': 'Alignment Scoring'
        }
        
        
submit_widgets = {
            'submitter_name': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'submitter_email': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'common_name': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'synonym': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'inchi': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'formula': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'nmr_references': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'nmr_solvent': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'mhz': forms.NumberInput(attrs={'class':"form-control", 'type':'number'}),
            'calibration': forms.TextInput(attrs={'class':"form-control", 'type':'text'}),
            'notes': forms.Textarea(attrs={'class':"form-control"}),
            'proton_spectrum': forms.Textarea(attrs={'class':"form-control"}),
            'carbon_spectrum': forms.Textarea(attrs={'class':"form-control"}),
            }

class SubmitForm(forms.models.ModelForm):

    class Meta:
        model = Submission
        #fields = query_fields
        widgets = submit_widgets
        labels = {
                'nmr_references' : 'NMR references',
                'nmr_solvent' : 'NMR solvent',
                'inchi': 'InChI',
                'mhz': 'mHz'
            }
            # 'proton_shift_tolerance': 'Proton Shift Tolerance', 'carbon_shift_tolerance': 'Carbon Shift Tolerance', 'coupling_constant_tolerance': 'Coupling Constant Tolerance', 'exact_mass': 'Exact Mass', 'chemical_formula': 'Chemical Formula', 
            # 'proton_nmr': 'Proton NMR', 'carbon_nmr': 'Carbon NMR',
        # }
