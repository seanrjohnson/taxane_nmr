#~ from django.core.management.base import BaseCommand, CommandError
#~ from nmr.models import Query, Result, Setting, Record, Proton, Carbon
#~ import zmq
#~ from django.conf import settings
#~ from multiprocessing import Process # the workers should run as separate processes
#~ #from threading import Thread # the request server can be in the same process as the rest of the django app because it is relatively lightweight
#~ import time
#~ import pickle
#~ import atexit
#~ import signal
#~ import sys
#~ from collections import deque
#~ from django.db.models import Q
#~ from nmr import util
#~ from decimal import Decimal
#~ #from pprint import pprint
#~ from nmr import search_routines
#~ import timeit
#~ 
#~ def execute_query(query_id):
    #~ #TODO add a cleanup routine here so that every so often we delete expired results
    #~ 
    #~ matching_function = search_routines.munkres_numpy
    #~ #matching_function = search_routines.munkres
    #~ 
    #~ errors_list = list()
    #~ query = None
    #~ try:
        #~ query = Query.objects.get(pk=query_id)
    #~ except Query.DoesNotExist:
        #~ errors_list.append("Could not find requested query")
    #~ hits = list()
    #~ if query:
        #~ validated_search_criteria = set() #if a search criteria was actually used in the database query, add it to this set
        #~ possible_hits = Record.objects.all()
        #~ if query.chemical_formula != "":
            #~ possible_hits = possible_hits.filter(formula=query.chemical_formula)
            #~ validated_search_criteria.add('formula')
        #~ if query.exact_mass != None:
            #~ if query.exact_mass_tolerance == None:
                #~ errors_list.append("Exact mass tolerance not specified. Exact mass not used as search criterion.")
            #~ else:
                #~ possible_hits = possible_hits.filter(exact_mass__lte=query.exact_mass + query.exact_mass_tolerance, exact_mass__gte=query.exact_mass - query.exact_mass_tolerance)
                #~ validated_search_criteria.add('exact_mass')
        #~ if query.proton_nmr:
            #~ if query.proton_shift_tolerance == None:
                #~ errors_list.append("Proton shift tolerance not specified. Proton nmr not used as search criterion.")
            #~ else:
                #~ (proton_parse_error, parsed_protons) = util.parse_proton_query(query.proton_nmr)
                #~ #pprint(parsed_protons)
                #~ errors_list += proton_parse_error
                #~ validated_search_criteria.add('proton_nmr')
                #~ if len(parsed_protons) > 0:
                    #~ 
                    #~ for (i, proton_dict) in enumerate(parsed_protons):
                        #~ ppm = proton_dict['ppm']
                        #~ if i == 0:
                            #~ composite_Q = Q(ppm__gte=ppm - query.proton_shift_tolerance) & Q(ppm__lte=ppm + query.proton_shift_tolerance)
                        #~ else:
                            #~ composite_Q = composite_Q | (Q(ppm__gte=ppm - query.proton_shift_tolerance) & Q(ppm__lte=ppm + query.proton_shift_tolerance))
                    #~ records_with_eligible_protons = Proton.objects.filter(composite_Q).values_list('record', flat=True).distinct() #this will execute a query
                    #~ possible_hits = possible_hits.filter(pk__in=records_with_eligible_protons)
        #~ if query.carbon_nmr:
            #~ if query.carbon_shift_tolerance == None:
                #~ errors_list.append("Carbon shift tolerance not specified. Carbon nmr not used as search criterion.")
            #~ else:
                #~ (carbon_parse_error, parsed_carbons) = util.parse_carbon_query(query.carbon_nmr)
                #~ errors_list += carbon_parse_error
                #~ if len(parsed_carbons) > 0:
                        #~ validated_search_criteria.add('carbon_nmr')
                        #~ for (i, ppm) in enumerate(parsed_carbons):
                            #~ if i == 0:
                                #~ composite_carbon_Q = Q(ppm__gte=ppm - query.carbon_shift_tolerance) & Q(ppm__lte=ppm + query.carbon_shift_tolerance)
                            #~ else:
                                #~ composite_carbon_Q = composite_carbon_Q | (Q(ppm__gte=ppm - query.proton_shift_tolerance) & Q(ppm__lte=ppm + query.proton_shift_tolerance))
                        #~ records_with_eligible_carbons = Carbon.objects.filter(composite_carbon_Q).values_list('record', flat=True).distinct() #this will execute a query
                        #~ possible_hits = possible_hits.filter(pk__in=records_with_eligible_carbons)
        #~ hits = possible_hits
    #~ results = list() #list of dicts: {lab_num:record lab number , score: alignment score, proton: proton peak matching or {}, carbon: carbon peak matching or {}}
    #~ hit_nums = list()
    #~ running_worst = list()
    #~ running_best = list()
    #~ running_score = list()
    #~ proton_assignments_list = list()
    #~ carbon_assignments_list = list()
    #~ for (i, hit) in enumerate(hits):
        #~ hit_nums.append(hit.lab_num)
        #~ results.append({'lab_num':hit.lab_num, 'formula':hit.formula, 'exact_mass':hit.exact_mass, 'common_name': hit.common_name})
        #~ running_best.append(0)
        #~ proton_assignments_list.append(list()) #default empty list
        #~ carbon_assignments_list.append(list()) #default empty list
        #~ if 'exact_mass' in validated_search_criteria:
            #~ running_worst.append(query.exact_mass_tolerance**2)
            #~ #print(query.exact_mass_tolerance**2)
            #~ #print((hit.exact_mass - query.exact_mass)**2)
            #~ running_score.append((hit.exact_mass - query.exact_mass)**2)
        #~ else:
            #~ running_worst.append(0)
            #~ running_score.append(0)
        #~ if 'proton_nmr' not in validated_search_criteria:
            #~ results[i]['proton']={}
        #~ if 'carbon_nmr' not in validated_search_criteria:
            #~ results[i]['carbon']={}
    #~ 
    #~ #TODO: implement different search routines and scoring styles
    #~ # run spectral alignment
    #~ if 'proton_nmr' in validated_search_criteria:
        #~ all_protons = Proton.objects.filter(record__in=hit_nums).order_by('record','order')
        #~ protons_sorted_by_record = dict() #a dict of lists, key is parent record id, value is a list of indexes of all_protons
        #~ query_protons_list = [x['ppm'] for x in parsed_protons]
        #~ for (i, proton) in enumerate(all_protons):
            #~ if proton.record.lab_num not in protons_sorted_by_record:
                #~ protons_sorted_by_record[proton.record.lab_num] = list()
            #~ protons_sorted_by_record[proton.record.lab_num].append(proton.ppm)
            #~ #if it has no protons, it won't even be the hit list, so there should be exactly as many keys in protons_sorted_by_record, as there are hits in hits
        #~ for (i, hit) in enumerate(hits):
            #~ #pprint(query_protons_list)
            #~ (proton_cost, worst_proton_cost, proton_assignment) = matching_function(query_protons_list, protons_sorted_by_record[hit.lab_num], query.proton_shift_tolerance)
            #~ running_score[i] += Decimal(proton_cost)
            #~ running_worst[i] += Decimal(worst_proton_cost)
            #~ proton_assignments_list[i] = proton_assignment
            #~ 
    #~ 
    #~ if 'carbon_nmr' in validated_search_criteria:
        #~ all_carbons = Carbon.objects.filter(record__in=hit_nums).order_by('record','order')
        #~ carbons_sorted_by_record = dict() #a dict of lists, key is parent record id, value is a list of indexes of all_carbons
        #~ query_carbons_list = parsed_carbons
        #~ for (i, carbon) in enumerate(all_carbons):
            #~ if carbon.record.lab_num not in carbons_sorted_by_record:
                #~ carbons_sorted_by_record[carbon.record.lab_num] = list()
            #~ carbons_sorted_by_record[carbon.record.lab_num].append(carbon.ppm)
            #~ #if it has no carbons, it won't even be the hit list, so there should be exactly as many keys in carbons_sorted_by_record, as there are hits in hits
        #~ for (i, hit) in enumerate(hits):
            #~ #pprint(query_protons_list)
            #~ (carbon_cost, worst_carbon_cost, carbon_assignment) = matching_function(query_carbons_list, carbons_sorted_by_record[hit.lab_num], query.carbon_shift_tolerance)
            #~ running_score[i] += Decimal(carbon_cost)
            #~ running_worst[i] += Decimal(worst_carbon_cost)
            #~ carbon_assignments_list[i] = carbon_assignment
    #~ for i in range(len(hits)):
        #~ results[i]['proton_assignment'] = proton_assignments_list[i]
        #~ results[i]['carbon_assignment'] = carbon_assignments_list[i]
    #~ 
    #~ #calculate scores
    #~ for (i, hit) in enumerate(hits):
        #~ if running_worst[i] != running_best[i]:
            #~ results[i]['score']=(running_worst[i]-running_score[i])*Decimal(100)/(running_worst[i] - running_best[i])
        #~ else:
            #~ results[i]['score'] = Decimal(100) #TODO: figure out an intelligent way to handle this case!
        #~ results[i]['score'] = results[i]['score'].quantize(Decimal('1.00'))
    #~ 
    #~ #save the results
    #~ results = sorted(results, key=lambda x: x['score'], reverse=True)
    #~ if len(validated_search_criteria) == 0:
        #~ errors_list.append("No valid search criteria entered, showing all database contents")
    #~ result = Result()
    #~ result.hash = query_id
    #~ result.errors = pickle.dumps(errors_list)
    #~ result.data = pickle.dumps(results)
    #~ result.save()
