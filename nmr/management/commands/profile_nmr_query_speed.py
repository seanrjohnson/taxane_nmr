import cProfile
from .execute_query import execute_query
from cProfile import Profile
from optparse import make_option

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        profiler = Profile()
            
        profiler.runcall(self._handle, *args, **options)
        profiler.print_stats()

    def _handle(self, *args, **options):
        execute_query("778ffa253333478d3135ba21631fb301fe59a321")