from __future__ import unicode_literals
from django.core.management.base import BaseCommand, CommandError
import zmq
from django.conf import settings
from nmr.models import Setting

class Command(BaseCommand):
    '''
        Kills the query server and all workers (you can restart it by running initialize_query_server)
    '''
    help = "Kills the query server and all workers (you can restart it by running initialize_query_server)"
    def handle(self, *args, **options):
        server_url = Setting.objects.get(name=settings.KEY_QUERY_SERVER_PORT).value
        context = zmq.Context()
        socket = context.socket(zmq.REQ)
        socket.connect(server_url)
        
        socket.send("die".encode('ascii')) #is this a secure mechanism? could some idiot send the die command over the internet?
        socket.recv() #we don't really care what we get back...
        socket.close()
        context.term()