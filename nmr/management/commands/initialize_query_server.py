from django.core.management.base import BaseCommand, CommandError
from nmr.models import Query, Result, Setting, Record, Proton, Carbon
import zmq
from django.conf import settings
from multiprocessing import Process # the workers should run as separate processes
from threading import Thread # the request server can be in the same process as the rest of the django app because it is relatively lightweight
import time
import pickle
import atexit
import signal
import sys
from collections import deque
from django.db.models import Q
from nmr import util
from decimal import Decimal
from pprint import pprint
from nmr import search_routines
import numpy
from django.utils import timezone
import sqlite3
from pandas.io import sql

def grab_and_sort_all_records():
    '''
        returns a set of all records, protons, carbons, and coupling constants from the database
        
        records is a pandas table, protons and carbons are dicts of pandas tables where the key is the record number, coupling_constants is a dict of pandas tables where the key is the proton id
    '''

    # Create your connection.
    #cnx = sqlite3.connect('db.sqlite3')
    cnx = sqlite3.connect('/taxane_nmr/taxane_nmr/db.sqlite3')
    tables = {'nmr_proton':None, 'nmr_carbon':None, 'nmr_record':None, 'nmr_protoncoupling':None} #keys are sql table names, values will be filled in with pandas data frames
    protons = dict()
    carbons = dict()
    coupling_constants = dict()
    
    for table_name in tables:
        tables[table_name] = sql.read_sql("SELECT * FROM %s;" % table_name, cnx)
    cnx.close()
    for num in tables['nmr_record']['lab_num']:
        protons[num] = tables['nmr_proton'][tables['nmr_proton']['record_id'] == num]
        carbons[num] = tables['nmr_carbon'][tables['nmr_carbon']['record_id'] == num]
        coupling_constants[num] = list()
        for proton in protons[num]['id']:
            coupling_constants[num].append( sorted([x['j'] for (i,x) in tables['nmr_protoncoupling'][tables['nmr_protoncoupling']['proton_id'] == proton].iterrows()])) # coupling_constants[x] = a list of lists of proton coupling_constants
    # for num in tables['nmr_proton']['id']:
        # shifts[num] = tables['nmr_protoncoupling'][tables['nmr_protoncoupling']['proton_id'] == num]
    return (tables['nmr_record'], protons, carbons, coupling_constants)
    


def worker_process(socket_url):
    '''
        Opens a connection to the queue server. Then sends a message to the server letting it know it can take jobs. When a job comes in, it gets executed with execute_query.
        Then the ready message is sent back to the server.
        socket_url is the url of the server
    '''
    (records, protons, carbons, coupling_constants) = grab_and_sort_all_records()
    print("worker started")
    def calculate_score(score, worst, best):
        '''
            all inputs should be type float
        '''
        out = None
        score = score
        worst = worst
        best = best
        if worst != best:
            out=(worst-score)*100/(worst - best)
        else:
            out=100
        #print(out)
        #out = out.quantize(Decimal('1.00'))
        return out
    
    def calculate_score_simple(score, worst):
        '''
            all inputs should be type float
        '''
        out = None
        score = score
        worst = worst
        if worst != 0:
            out = score*100/worst
        else:
            out=100
        return out
    #def x_in_y(x,y)

    def execute_query(query_id):
        print("query start")
        start_time = time.clock()
        matching_function = search_routines.munkres_numpy
        #matching_function = search_routines.munkres
        
        errors_list = list()
        query = None
        try:
            query = Query.objects.get(pk=query_id)
        except Query.DoesNotExist:
            errors_list.append("Could not find requested query")
        hits = list()
        if query:
            validated_search_criteria = set() #if a search criteria was actually used in the database query, add it to this set
            possible_hits = records
            exact_masses = []
            if query.chemical_formula != "":
                chemical_formulas = query.chemical_formula.split(";")
                possible_hits = possible_hits[possible_hits['formula'].isin(chemical_formulas)]
                validated_search_criteria.add('formula')
            if query.name.strip() != "":
                query_name = query.name.strip().upper()
                possible_hits = possible_hits[possible_hits['common_name'].map(lambda x: query_name in x.upper())]
                validated_search_criteria.add('name')
            if query.exact_mass != None and query.exact_mass != "":
                if query.exact_mass_tolerance == None:
                    errors_list.append("Exact mass tolerance not specified. Exact mass not used as search criterion.")
                else:
                    selector = possible_hits['exact_mass'] != possible_hits['exact_mass']
                    exact_masses = [float(x.strip()) for x in query.exact_mass.split(";")]
                    for exact_mass in query.exact_mass.split(";"):
                        exact_mass = Decimal(exact_mass.strip())
                        selector = selector | ((possible_hits['exact_mass'] <= exact_mass + query.exact_mass_tolerance) & (possible_hits['exact_mass'] >= exact_mass - query.exact_mass_tolerance))

                    possible_hits = possible_hits[ selector ]
                    validated_search_criteria.add('exact_mass')
            if query.proton_nmr:
                if query.proton_shift_tolerance == None:
                    errors_list.append("Proton shift tolerance not specified. Proton nmr not used as search criterion.")
                else:
                    (proton_parse_error, parsed_protons) = util.parse_proton_query(query.proton_nmr)
                    errors_list += proton_parse_error
                    if len(parsed_protons) > 0:
                        validated_search_criteria.add('proton_nmr')
                        # for (i, proton_dict) in enumerate(parsed_protons):
                            # ppm = proton_dict['ppm']
                            # if i == 0:
                                # composite_Q = Q(ppm__gte=ppm - query.proton_shift_tolerance) & Q(ppm__lte=ppm + query.proton_shift_tolerance)
                            # else:
                                # composite_Q = composite_Q | (Q(ppm__gte=ppm - query.proton_shift_tolerance) & Q(ppm__lte=ppm + query.proton_shift_tolerance))
                        # records_with_eligible_protons = Proton.objects.filter(composite_Q).values_list('record', flat=True).distinct() #this will execute a query
                        # possible_hits = possible_hits.filter(pk__in=records_with_eligible_protons)
            if query.carbon_nmr:
                if query.carbon_shift_tolerance == None:
                    errors_list.append("Carbon shift tolerance not specified. Carbon nmr not used as search criterion.")
                else:
                    (carbon_parse_error, parsed_carbons) = util.parse_carbon_query(query.carbon_nmr)
                    errors_list += carbon_parse_error
                    if len(parsed_carbons) > 0:
                            validated_search_criteria.add('carbon_nmr')
                            # for (i, ppm) in enumerate(parsed_carbons):
                                # if i == 0:
                                    # composite_carbon_Q = Q(ppm__gte=ppm - query.carbon_shift_tolerance) & Q(ppm__lte=ppm + query.carbon_shift_tolerance)
                                # else:
                                    # composite_carbon_Q = composite_carbon_Q | (Q(ppm__gte=ppm - query.proton_shift_tolerance) & Q(ppm__lte=ppm + query.proton_shift_tolerance))
                            # records_with_eligible_carbons = Carbon.objects.filter(composite_carbon_Q).values_list('record', flat=True).distinct() #this will execute a query
                            # possible_hits = possible_hits.filter(pk__in=records_with_eligible_carbons)
            hits = possible_hits.reset_index(drop=True)
        results = list() #list of dicts: {lab_num:record lab number , score: alignment score, proton: proton peak matching or {}, carbon: carbon peak matching or {}}
        hit_nums = list()
        #running_worst = list()
        #running_best = list()
        #running_score = list()
        proton_assignments_list = list()
        proton_unmatched_list = list()
        carbon_assignments_list = list()
        carbon_unmatched_list = list()
        for (i, hit) in hits.iterrows():
            hit_nums.append(hit['lab_num'])
            results.append({'lab_num':hit['lab_num'], 'formula':hit['formula'], 'exact_mass':hit['exact_mass'], 'common_name': hit['common_name']})
            #running_best.append(0)
            proton_assignments_list.append(list()) #default empty list
            carbon_assignments_list.append(list()) #default empty list
            proton_unmatched_list.append(list()) #default empty list
            carbon_unmatched_list.append(list()) #default empty list
            if 'exact_mass' in validated_search_criteria:
                #running_worst.append(query.exact_mass_tolerance**2)
                #print(query.exact_mass_tolerance**2)
                #print((hit.exact_mass - query.exact_mass)**2)
                #running_score.append((hit['exact_mass'] - float(query.exact_mass))**2)
                
                #print("len: " + len(results))
                (closest, distance) = get_closest_from_list(hit['exact_mass'], exact_masses)
                results[i]['exact_mass_score'] = calculate_score(distance**2, float(query.exact_mass_tolerance)**2, 0)
            else:
                #running_worst.append(0)
                #running_score.append(0)
                results[i]['exact_mass_score'] = None
            if 'proton_nmr' not in validated_search_criteria:
                results[i]['proton']={}
                results[i]['proton_score'] = None
            if 'carbon_nmr' not in validated_search_criteria:
                results[i]['carbon']={}
                results[i]['carbon_score'] = None
        print("preliminary done", time.clock()-start_time)
        start_time = time.clock()
        #TODO: implement different search routines and scoring styles
        # run spectral alignment
        proton_its = 0
        if 'proton_nmr' in validated_search_criteria:
            #all_protons = Proton.objects.filter(record__in=hit_nums).order_by('record','order')
            #protons_sorted_by_record = dict() #a dict of lists, key is parent record id, value is a list of indexes of all_protons
            query_protons = dict()
            query_protons['ppm'] = [x['ppm'] for x in parsed_protons]
            query_protons['peak_type'] = [x['peak_type'] for x in parsed_protons]
            query_coupling_constants_specified = False
            query_protons['j'] = list() #list of lists of coupling constants
            for x in parsed_protons:
                if len(x['j']) > 0:
                    query_coupling_constants_specified = True
                query_protons['j'].append(x['j'])
            
            # for (i, proton) in enumerate(all_protons):
                # if proton.record.lab_num not in protons_sorted_by_record:
                    # protons_sorted_by_record[proton.record.lab_num] = (list(), list(), list()) # protons_sorted_by_record[lab_num][ppm=0|type=1|j=2][position], if the middle index is 2 (j), then values are lists of coupling constants, if 0 they are decimals, if 1 they are strings
                # protons_sorted_by_record[proton.record.lab_num][0].append(proton.ppm)
                # protons_sorted_by_record[proton.record.lab_num][1].append(proton.peak_type_search_format)
                # protons_sorted_by_record[proton.record.lab_num][2].append(sorted([x.j for x in proton.protoncoupling_set.all()]))
                # #if it has no protons, it won't even be the hit list, so there should be exactly as many keys in protons_sorted_by_record, as there are hits in hits
            for (i, hit) in hits.iterrows():
                #pprint(query_protons_list)
                #print(hit['lab_num'])
                if len(protons[hit['lab_num']].index) > 0:
                    peak_type_mask = search_routines.matrix_mask_string(query_protons['peak_type'], protons[hit['lab_num']]['peak_type_search_format'])
                    if query_coupling_constants_specified and query.coupling_constant_tolerance == None:
                        errors_list.append("Proton coupling constant tolerance not specified. Proton coupling not used as a search criterion.")
                        peak_j_mask = numpy.empty(len(query_protons['j']), len(coupling_constants[hit['lab_num']]), dtype=bool)
                        peak_j_mask.fill(True)
                    else:
                        peak_j_mask = search_routines.matrix_mask_array(query_protons['j'], coupling_constants[hit['lab_num']], query.coupling_constant_tolerance)
                    composite_mask = numpy.logical_and(peak_type_mask, peak_j_mask)
                    #pprint(composite_mask)
                    (proton_matches, possible_proton_matches, proton_assignment, proton_unmatched) = matching_function(query_protons['ppm'], protons[hit['lab_num']]['ppm'].values, query.proton_shift_tolerance, composite_mask, extra_query_fields={'query_j': util.catenate_coupling_constant_lists(query_protons['j']), 'query_type': query_protons['peak_type']}, extra_database_fields={'position': protons[hit['lab_num']]['position'].values, 'database_j': util.catenate_coupling_constant_lists(coupling_constants[hit['lab_num']]), 'database_type': protons[hit['lab_num']]['peak_type'].values})
                    if query.alignment_scoring == "PURE":
                        denominator = max(float(len(protons[hit['lab_num']]['ppm'].values)), float(len(query_protons['ppm'])))
                    else:
                        denominator = float(len(protons[hit['lab_num']]['ppm'].values))
                    results[i]['proton_score'] = calculate_score_simple(proton_matches, denominator)
                    proton_its += 1
                    #print(proton_cost)
                    #running_score[i] += proton_cost
                    #running_worst[i] += worst_proton_cost
                    proton_assignments_list[i] = proton_assignment
                    proton_unmatched_list[i] = sorted(proton_unmatched)
                else:
                    results[i]['proton_score'] = 0
                
        print("proton done", time.clock()-start_time, proton_its)
        start_time = time.clock()
        carbon_its = 0
        if 'carbon_nmr' in validated_search_criteria:
            #all_carbons = Carbon.objects.filter(record__in=hit_nums).order_by('record','order')
            #carbons_sorted_by_record = dict() #a dict of lists, key is parent record id, value is a list of indexes of all_carbons
            query_carbons = parsed_carbons
            # for (i, carbon) in enumerate(all_carbons):
                # if carbon.record.lab_num not in carbons_sorted_by_record:
                    # carbons_sorted_by_record[carbon.record.lab_num] = list()
                # carbons_sorted_by_record[carbon.record.lab_num].append(carbon.ppm)
                # #if it has no carbons, it won't even be the hit list, so there should be exactly as many keys in carbons_sorted_by_record, as there are hits in hits
            for (i, hit) in hits.iterrows():
                if len(carbons[hit['lab_num']].index) > 0:
                    (carbon_matches, possible_carbon_matches, carbon_assignment, carbon_unmatched) = matching_function(query_carbons, carbons[hit['lab_num']]['ppm'].values, query.carbon_shift_tolerance, extra_database_fields={'position': carbons[hit['lab_num']]['position'].values})
                    carbon_its += 1
                    if query.alignment_scoring == "PURE":
                        denominator = max(float(len(carbons[hit['lab_num']]['ppm'].values)), float(len(query_carbons)))
                    else:
                        denominator = float(len(carbons[hit['lab_num']]['ppm'].values))
                    results[i]['carbon_score'] = calculate_score_simple(carbon_matches, denominator)
                    #print(results[i]['carbon_score'])
                    # running_score[i] += carbon_cost
                    # running_worst[i] += worst_carbon_cost
                    carbon_assignments_list[i] = carbon_assignment
                    carbon_unmatched_list[i] = sorted(carbon_unmatched)
                else:
                    results[i]['carbon_score'] = 0
        for i in range(len(hits)):
            results[i]['proton_assignment'] = proton_assignments_list[i]
            results[i]['carbon_assignment'] = carbon_assignments_list[i]
            results[i]['proton_unmatched'] = proton_unmatched_list[i]
            results[i]['carbon_unmatched'] = carbon_unmatched_list[i]
        print("carbon done", time.clock()-start_time, carbon_its)
        start_time = time.clock()
        #calculate scores
        for (i, hit) in hits.iterrows():
            running_numerator = 0
            running_denominator = 0
            for s in ['carbon_score', 'proton_score', 'exact_mass_score']:
                #print(s)
                #print(results[i][s])
                if results[i][s] is not None:
                    running_numerator += results[i][s]
                    running_denominator += 1
            if running_denominator != 0:
                results[i]['score'] = running_numerator/running_denominator
            else:
                results[i]['score'] = 100
            #results[i]['score'] = 
            # if running_worst[i] != running_best[i]:
                # results[i]['score']=(running_worst[i]-running_score[i])*Decimal(100)/(running_worst[i] - running_best[i])
            # else:
                # results[i]['score'] = Decimal(100) #TODO: figure out an intelligent way to handle this case!
            #results[i]['score'] = results[i]['score'].quantize(Decimal('1.00'))
        
        #save the results
        results = sorted(results, key=lambda x: x['score'], reverse=True)
        if len(validated_search_criteria) == 0:
            errors_list.append("No valid search criteria entered, showing all database contents")
        result = Result()
        result.query = query
        result.hash = query_id
        result.errors = pickle.dumps(errors_list)
        result.data = pickle.dumps(results)
        result.save()
        print("query done")
    
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect(socket_url)
    
    def remove_old_records():
        expired_queries = Query.objects.filter(result__expires__lt=timezone.now())
        expired_queries.delete() #this will also delete the result objects
        
    
    try:
        while True:
            socket.send(b"1") #doesn't matter what we send, we're after the database side effects
            request = socket.recv().decode('ascii') #get the request and convert it to text
            if request == 'clean':
                remove_old_records()
            else:
                execute_query(request) #execute the request as a job then loop back around and get another one
            
        
    except KeyboardInterrupt:
        raise
def get_closest_from_list(number, list_of_numbers):
    closest = float("inf")
    distance = float("inf")
    for num in list_of_numbers:
        new_dist = abs(number-num)
        if new_dist < distance:
            closest = num
            distance = new_dist
    return closest, distance

def initialize_server():
    '''
        Open two sockets, one to send jobs to workers, and one to receive jobs from clients. 
        Creates a number of workers, each on a new thread, equal to the number specified in the app settings.
        Requests arrive in the form of single ascii encoded byte strings that give the id of the query to execute.
        When a request arrives, the query id is put into a queue and a reply is immediately sent to the client
        (so that submitting a job doesn't cause the client to block for very long). When there is an available worker,
        the next job on the queue is assigned to it.
    '''
    #open job router sockets
    context = zmq.Context()
    client_url = "tcp://127.0.0.1:0" #port zero means ask the OS for any open port.
    worker_url = "tcp://127.0.0.1:0"
    client_socket = context.socket(zmq.ROUTER)
    client_socket.bind(client_url)
    client_url = client_socket.getsockopt_string(zmq.LAST_ENDPOINT)
    worker_socket = context.socket(zmq.ROUTER)
    worker_socket.bind(worker_url)
    worker_url = worker_socket.getsockopt_string(zmq.LAST_ENDPOINT)
    
    # save the location of the socket opened for communicating with clients in the database
    client_url_setting = Setting.objects.get_or_create(name=settings.KEY_QUERY_SERVER_PORT)[0]
    client_url_setting.value = client_url
    client_url_setting.save()
    
    # create the workers
    workers = list()
    for i in range(settings.QUERY_WORKERS):
        p = Process(target=worker_process, args=(worker_url,))
        p.start()
        workers.append(p)
    #try to minimize weird hanging and memory leaks when we kill or close the server
    atexit.register(lambda: [x.terminate() for x in workers]) #doesn't seem to matter for keyboard interrupt, ctrl-break kills them all dead
    
    poller = zmq.Poller()
    poller.register(client_socket, zmq.POLLIN)
    poller.register(worker_socket, zmq.POLLIN)
    job_queue = deque()
    #available_workers = 0
    available_workers_list = deque()
    job_counter = 0
    kill = False #if true, finish whatever's in the queue, and then quit
    try:
        while True:
            if kill: #die
                if len(available_workers_list) == settings.QUERY_WORKERS and len(job_queue) == 0:
                    return
            
            socks = dict(poller.poll())

            

                
            if not kill: #listen for more query requests
                if (client_socket in socks and socks[client_socket] == zmq.POLLIN):
                    # A view is requesting a search
                    
                    #recieve the three parts of the client request
                    client_addr = client_socket.recv()
                    empty = client_socket.recv()
                    assert empty == b""
                    request = client_socket.recv()
                    
                    if request == "die":
                        kill = True
                    else: #enqueue the client request
                        job_queue.append(request)
                    
                    #send a message to the client telling it all is well and that the request has been enqueued
                    client_socket.send(client_addr, zmq.SNDMORE)
                    client_socket.send(b"", zmq.SNDMORE)
                    client_socket.send(b"1")
            
            if (worker_socket in socks and socks[worker_socket] == zmq.POLLIN):
                worker_addr = worker_socket.recv()
                
                #available_workers += 1
                
                available_workers_list.append(worker_addr)
                
                #just ignore the next parts of the message
                empty = worker_socket.recv()
                #assert empty == b""
                msg = worker_socket.recv()
            
            if len(available_workers_list) > 0 and len(job_queue) > 0:
                job = job_queue.popleft()
                worker = available_workers_list.popleft()
                worker_socket.send(worker, zmq.SNDMORE)
                worker_socket.send(b"", zmq.SNDMORE)
                worker_socket.send(job)
                job_counter += 1
                if job_counter > settings.QUERIES_BETWEEN_CLEANING:
                    job_counter = 0
                    job_queue.append(b'clean')

                    
                

                
    except KeyboardInterrupt:
        raise
        
        
class Command(BaseCommand):
    args = ""
    help = "initializes a task queue that will accept and execute search job requests"
    def handle(self, *args, **options):
        Thread(target=initialize_server).start()
        
    


    
