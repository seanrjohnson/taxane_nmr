from __future__ import unicode_literals
from django.core.management.base import BaseCommand, CommandError
import zmq
from django.conf import settings
from nmr.models import Setting

class Command(BaseCommand):
    '''
        Given a space separated list of query IDs, submit them to the job queue for execution
    '''
    args = "query_hash"
    help = "Sends a query hash to the job queue to execute a database search on"
    def handle(self, *args, **options):
        server_url = Setting.objects.get(name=settings.KEY_QUERY_SERVER_PORT).value
        context = zmq.Context()
        socket = context.socket(zmq.REQ)
        #print(server_url)
        socket.connect(server_url)
        
        for query_hash in args:
            socket.send(query_hash.encode('ascii'))
            socket.recv() #we don't really care what we get back...
        socket.close()
        context.term()