from nmr.munkres import Munkres
from pprint import pprint
from nmr.munkres_numpy import linear_assignment
import numpy

# in all matrices in this library, rows(the first index) are database peaks, and colums(the second index) are query peaks
munkres_class = Munkres()

def least_squares_matrix_numpy(rows, columns, tolerance):
    out = numpy.array(least_squares_matrix(rows, columns, tolerance),dtype=float)
    #print(out)
    return out


def pad_numpy_to_square(a, pad_value=0):
    #http://stackoverflow.com/questions/10871220/making-a-matrix-square-and-padding-it-with-desired-value-in-numpy
    m = a.reshape((a.shape[0], -1))
    padded = pad_value * numpy.ones(2 * [max(m.shape)], dtype=m.dtype)
    padded[0:m.shape[0], 0:m.shape[1]] = m
    return padded
    
def least_squares_matrix(rows, columns, tolerance):
    '''
        Takes two lists of floats and generates a matrix of the squared distances between members of different lists.
        Entries have a maximum value of tolerance^s. If any distance is greater than tolerance, the score will be 
        set to tolerance^2
    '''
    out = list()
    tolerance_squared = float(tolerance)**2
    #pprint(query)
    #pprint(database)
    for (i, q) in enumerate(rows):
        out.append(list())
        for (j, d) in enumerate(columns):
            squared_dist = (float(q) - float(d))**2
            if squared_dist > tolerance_squared:
                out[i].append(tolerance_squared)
            else:
                out[i].append(squared_dist)
    
    out = munkres_class.pad_matrix(out, tolerance_squared)
    return out

def matrix_mask_string(query, database):
    '''
        Compares entries of query and database and generate a comparison matrix (2d numpy array), 1 if two entries are similar, 0 if different
        query and database are lists of strings
    '''
    out = numpy.zeros((len(database), len(query)), dtype=bool)
    for (i, q) in enumerate(query):
        for (j, d) in enumerate(database):
            if q == d or q == "" or d == "":
                out[j, i] = True
            
    return out
def matrix_mask_array(query, database, tolerance):
    '''
        Compares entries of query and database and generate a comparison matrix (2d numpy array), 1 if two entries are similar, 0 if different
        query and database are lists of lists of numbers. tolerance is how close entries must be to be considered equal
    '''
    out = numpy.zeros((len(database), len(query)), dtype=bool)
    for (i, q) in enumerate(query):
        for (j, d) in enumerate(database):
            same = True
            if len(q) == 0 or len(d) == 0 :
                pass #same = True, it may be that the values just weren't entered, so don't penalize for that
            elif len(q) != len(d):
                same = False
            else:
                #q = sorted(q)
                #d = sorted(d)
                for (k, e) in enumerate(q):
                    if (abs(d[k] - e) > tolerance):
                        same = False
            out[j, i] = same
    return out

def munkres_matrix(scoring_style, least_squares): #, ... masks!
    pass
    
# def munkres(query, database, tolerance):
    # scoring_matrix = least_squares_matrix(database, query, tolerance)
    # #pprint(scoring_matrix)
    # #TODO: mask and all of that stuff
    # result = munkres_class.compute(scoring_matrix)
    # cost = 0
    # tol_squared = tolerance**2
    # assignment = list()
    # database_len = len(database)
    # query_len = len(query)
    # for r, c in result:
        # x = scoring_matrix[r][c]
        # cost += x
        # if r < database_len:
            # if x < tol_squared and c < query_len:
                # assignment.append({'database_ppm':database[r], 'query_ppm':query[c], 'distance':abs(query[c]-database[r]), 'score':x})
            # else:
                # assignment.append({'database_ppm':database[r], 'query_ppm':"", 'score':x})
            
        
    # worst_cost = tol_squared * len(query) #TODO: maybe there's a better way to calculate this
    
    # return cost, worst_cost, assignment
def munkres_numpy(query, database, tolerance, mask=None, extra_query_fields={}, extra_database_fields={}, scoring="global"):
    '''
        query = list of query shifts
        database = list of database shifts
        tolerance = maximum distance between query and database to be considered a match
        mask = a boolean 2-d numpy array of dimensions len(database) X len(query). An entry of False in this matrix will mean that the corresponding peaks can't be 
                aligned to eachother
        extra_query_fields = a dict where keys are field names and values are lists of the same length as query, these are extra fields that will be associated with the peaks in
                            the alignment table
        extra_database_fields = like query fields, but for database entries
        scoring = 'global' or 'local' (default 'global') if global, scoring will be hits/max(len(query),len(database), else scoring will be hits/len(query)
    '''
    tolerance = float(tolerance)
    tol_squared = tolerance**2
    scoring_matrix = least_squares_matrix_numpy(database, query, tolerance)
    if mask is not None:
        mask = pad_numpy_to_square(mask, False)
        mask = numpy.logical_not(mask)
        scoring_matrix[mask] = tol_squared
    result = linear_assignment(scoring_matrix)
    cost = 0
    assignment = list()
    database_len = len(database)
    query_len = len(query)
    num_matches = 0
    unmatched = list()
    for r, c in result:
        x = scoring_matrix[r,c]
        cost += x
        assigned = False
        if r < database_len:
            assignment.append({'database_ppm':database[r], 'query_ppm':"", 'score':x,})
            for k, v in extra_database_fields.items():
                assignment[-1][k] = v[r]
            if x < tol_squared and c < query_len: #they match
                num_matches += 1
                assigned = True
                assignment[-1]['query_ppm'] = query[c]
                assignment[-1]['distance'] = abs(query[c]-database[r])
                for k, v in extra_query_fields.items():
                    assignment[-1][k] = v[c]
        if assigned == False and c < len(query):
            unmatched.append(query[c])
    
    worst_cost = float(max(len(query),len(database)))
    
    #return cost, worst_cost, assignment
    return num_matches, worst_cost, assignment, unmatched