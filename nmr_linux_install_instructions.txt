make sure apache 2.4 is installed

get python3 pip
sudo apt-get install python3-pip

get git
sudo apt-get install git

Install Django 1.6.11 (the last version of Django 1.6)
sudo pip3 install Django==1.6.11

Install the rest of the required python libraries
sudo pip3 install numpy
sudo pip3 install pandas
sudo pip3 install xlrd
sudo pip3 install openpyxl==1.8.2
sudo pip3 install pyzmq==14.1.0

install mod-wsgi
sudo apt-get install libapache2-mod-wsgi-py3

make a directory for the website I'll use /taxane_nmr
sudo mkdir /taxane_nmr
sudo chown sean:sean /taxane_nmr

grab taxane_nmr from the internet
git clone https://seanrjohnson@bitbucket.org/seanrjohnson/taxane_nmr.git

make a new file in /taxane_nmr/taxane_nmr/taxane_nmr (the directory that contains wsgi.py and settings.py)
call the new file private_settings.py
This file should define the following variables:
EMAIL_USE_TLS = 
EMAIL_HOST = 
EMAIL_HOST_USER = 
EMAIL_HOST_PASSWORD = 
EMAIL_PORT = 
ADMINS = (('admin_real_name', 'admin@email_address'),)
SPECTRUM_SUBMISSION_RECIEVERS = []

navigate to /taxane_nmr/taxane_nmr
python3 manage.py collectstatic

Generate the database:
if there is a file called "db.sqlite3", delete it.
run 
python3 manage.py syncdb
python3 make_db.py


somewhere in /etc/apache2/apache2.conf 
add the lines

WSGIScriptAlias /nmr /taxane_nmr/taxane_nmr/taxane_nmr/wsgi.py
WSGIPythonPath /taxane_nmr/taxane_nmr

in /etc/apache2/sites-available/000-default.conf
add the following lines to the default virtual host

<Directory /taxane_nmr/taxane_nmr/taxane_nmr>
	<Files wsgi.py>
		Require all granted
	</files>
</Directory>


Alias /nmr/static/ "/taxane_nmr/static/"

<Directory "/taxane_nmr/static/">
	Require all granted
</Directory>


Open up /taxane_nmr/taxane_nmr/taxane_nmr/settings.py
and make sure 
STATIC_URL = '/nmr/static/'
(the right side of that equation should be the same as the static alias directory in the apache configuration)

IMPORTANT!!!!!

also, make sure:
DEBUG = False


TEMPLATE_DEBUG = False

You should also replace the secret key to something other than what is in the repo (which is public!)
to generate a new secret run the generate_secret_key.py script included in the taxane_nmr directory:
python generate_secret_key.py

You should also add your domain name to ALLOWED_HOSTS, in my case:
ALLOWED_HOSTS = ['langelabtools.wsu.edu']


Change permissions for the files to allow the webserver to access and modify them


Change the path for the database file in /taxane_nmr/taxane_nmr/nmr/management/commands/initialize_query_server.py
line:
cnx = sqlite3.connect('db.sqlite3')
to:
cnx = sqlite3.connect('/taxane_nmr/taxane_nmr/db.sqlite3')


change the path to temporary files:
make a directory with everyone having write permissions:
/tmp/nmr_temp

in the settings file /taxane_nmr/taxane_nmr/taxane_nmr/settings.py, change:
TEMPFILE_DIRECTORY = os.path.join(BASE_DIR, '../temp')
to
TEMPFILE_DIRECTORY = "/tmp/nmr_temp"


in taxane_nmr/wsgi.py Comment out the line:
call_command('initialize_query_server')

The purpose of that line is to start the worker processes that run the searches.
It seems to randomly crash, however, which will put down the server if it's run as part of the server process.
That's bad!
The workaround is to make a scheduled task that starts up the backend whenever the computer starts.

add the following to crontab (by executing the command: crontab -e):

@reboot python3 /taxane_nmr/taxane_nmr/manage.py initialize_query_server